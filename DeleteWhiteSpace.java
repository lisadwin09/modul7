import java.util.Scanner;
public class DeleteWhiteSpace {
    public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Ketik kalimat apa saja : ");
		String word = scanner.nextLine();

        word = word.replaceAll("\\s", "");
        System.out.println("Sesudah spasi dihapus: " + word);
    }
}