public class StringMiscellaneous {
	public static void main(String[] args) {
		String sl = "hello there";
		char[] charArray = new char[5];
		
		System.out.printf("s1 : %s", sl);
		System.out.printf("\nLength of sl : %d", sl.length());
		System.out.printf("%nThe string reversed is: ");
		
		for (int count = sl.length() - 1; count >= 0; count-- )
			System.out.printf("%c ", sl.charAt(count));
		
		sl.getChars(0, 5, charArray, 0);
		System.out.printf("%nThe character array is: ");
		
		for (char character : charArray)
			System.out.print(character);
		
		System.out.println();
	}
}