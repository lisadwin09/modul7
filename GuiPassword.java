import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class GuiPassword extends JFrame implements ActionListener {
	JLabel label = new JLabel("Masukkan password : "); 
    JPanel panel = new JPanel();
    JPasswordField textfield = new JPasswordField("");
    JButton  button = new JButton("OK");
    
    public GuiPassword(String Title) {
		super("Validation Password");
		
		setSize(200, 120);
		setVisible(true);
		setContentPane(panel);
	    panel.setLayout(new BorderLayout());
		panel.add(label, BorderLayout.NORTH);
		panel.add(textfield, BorderLayout.CENTER);
		panel.add(button, BorderLayout.SOUTH);
		button.addActionListener(this);
	}
    
    public static boolean validatePassword(String textfield) {
		return textfield.matches("\\w{8}");
	    }
    
    public static boolean cekPassword(String textfield) {
		return textfield.matches("[A-Z]\\w{8}");
	    }
    
    public void actionPerformed(ActionEvent e) {
		if(!validatePassword(textfield.getText()))
			JOptionPane.showMessageDialog(null,"Maaf password tidak valid, harus 8 karakter");
		else if(!cekPassword(textfield.getText()))
			JOptionPane.showMessageDialog(null,"Maaf password tidak valid, huruf pertama harus kapital");	
		else 
			JOptionPane.showMessageDialog(null,"Password anda *******" +textfield.getText().charAt(7)+ " sudah dibuat");
    }
    
    public static void main(String args[]) {
        GuiPassword psw = new GuiPassword("coba");
    }
}