public class ValidateInput {
	//validasi first name
	public static boolean validateFirstName(String firstName) {
		return firstName.matches("[A-Z][a-zA-Z]*");
	}
	
	//validasi last name
	public static boolean validateLasttName(String lasttName) {
		return lasttName.matches("[a-zA-Z]+(['-][a-zA-Z]+)*");
	}
	
	//validasi alamat
	public static boolean validateAddress(String address) {
		return address.matches("\\d+\\s+([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)");
	}
	
	//validasi kota
	public static boolean validateCity(String city) {
		return city.matches("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z])+");
	}
	
	//validasi negara
	public static boolean validateState(String state) {
		return state.matches("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)");
	}
	
	//validasi kode pos
	public static boolean validateZip(String zip) {
		return zip.matches("\\d{5}");
	}
	
	//validasi telephone
	public static boolean validatePhone(String phone) {
		return phone.matches("[1-9]\\d{2}-[1-9]\\d{2}-\\d{4}");
	}
}