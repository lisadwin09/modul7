import java.util.Scanner;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DuplicateWord {  
    public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Ketik kalimat apa saja : ");
		String string = scanner.nextLine();

        int count;  
        string = string.toLowerCase();  
          
        System.out.println("Kata yang duplikat : ");   
		List<String> list = Arrays.asList(string.split(" "));

		Set<String> uniqueWords = new HashSet<String>(list);
		for (String word : uniqueWords) {
			if(Collections.frequency(list, word) > 1)
				System.out.println(word + ": " + Collections.frequency(list, word));
			else
				System.out.print("");
		}
	}
}