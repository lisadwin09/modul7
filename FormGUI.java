import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class FormGUI extends JFrame implements ActionListener {
	String firstname;
	String lastname;
	String address;
	String city;
	String state;
	String zip;
	String phone;
	JLabel label = new JLabel(); 
    JPanel panel = new JPanel();
    JTextField textfield1 = new JTextField();
	JTextField textfield2 = new JTextField();
	JTextField textfield3 = new JTextField();
	JTextField textfield4 = new JTextField();
	JTextField textfield5 = new JTextField();
	JTextField textfield6 = new JTextField();
	JTextField textfield7 = new JTextField();
    JButton  button = new JButton("OK");
    
    public FormGUI(String Title) {
		super("Validation Password");
		
		setSize(500,500);
        setVisible(true);
		setContentPane(panel);
	    panel.setLayout(new GridLayout(8,2));
		panel.add(new JLabel("Masukkan nama depan : "));
        panel.add(textfield1);
		panel.add(new JLabel("Masukkan nama belakang : "));
        panel.add(textfield2);
		panel.add(new JLabel("Masukkan alamat : "));
        panel.add(textfield3);
		panel.add(new JLabel("Masukkan kota : "));
        panel.add(textfield4);
		panel.add(new JLabel("Masukkan negara : "));
        panel.add(textfield5);
		panel.add(new JLabel("Masukkan ZIP : "));
        panel.add(textfield6);
		panel.add(new JLabel("Masukkan no telp : "));
        panel.add(textfield7);
		panel.add(button);
		button.addActionListener(this);
	}

    public void actionPerformed(ActionEvent e) {
 		if((!ValidateInput.validateFirstName(textfield1.getText()))||(!ValidateInput.validateLasttName(textfield2.getText()))||(!ValidateInput.validateAddress(textfield3.getText()))||(!ValidateInput.validateCity(textfield4.getText()))||(!ValidateInput.validateState(textfield5.getText()))||(!ValidateInput.validateZip(textfield6.getText()))||(!ValidateInput.validatePhone(textfield7.getText())))
 			JOptionPane.showMessageDialog(null,"Masukan tidak valid");
 		else
 			JOptionPane.showMessageDialog(null,"Terima kasih");
    }
    
    public static void main(String args[]) {
        FormGUI form = new FormGUI("Form");
    }
}